using System;
using UnityEngine;
using UnityEngine.UI;
using Network;

public class ClientExample : MonoBehaviour
{
    private const string channel = "dev-test";
    private string host = "http://td.3bodygame.com:10004/api";
    private Proto.ServerMetaParams serverParam;
    private string authToken = "";
    private string accessToken = "";
    private string username = "";

    private Button loginBtn;
    private Button matchBtn;
    private Button battleBtn;
    private Button loadAssetBtn;
    
    // Use this for initialization
    void Start()
    {
        loginBtn = transform.Find("LoginButton").GetComponent<Button>();
        matchBtn = transform.Find("MatchButton").GetComponent<Button>();
        battleBtn = transform.Find("BattleButton").GetComponent<Button>();
        loadAssetBtn = transform.Find("LoadButton").GetComponent<Button>();

        if (loginBtn != null)
        {
            loginBtn.onClick.AddListener(OnClickStartLogin);
        }
        if (matchBtn != null)
        {
            matchBtn.onClick.AddListener(OnClickStartMatch);
        }
        if (battleBtn != null)
        {
            battleBtn.onClick.AddListener(OnClickStartBattle);
        }
        if (loadAssetBtn != null)
        {
            loadAssetBtn.onClick.AddListener(OnLoadStreamingAsset);
        }
        Client.Initialize();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        Client.Update();
    }

    private void OnDestroy()
    {
        Client.Destroy();
    }

    void OnLoadStreamingAsset()
    {
        string filename = "data.json";
        StartCoroutine(FileUtil.LoadAssetContent(filename, OnLoadAssetCompleted));
    }

    void OnLoadAssetCompleted(bool ok, string content)
    {
        string msg = string.Format("{0}: {1}", ok, content);
        throw new InvalidOperationException(msg);
    }

    void OnClickStartLogin()
    {
        string url = string.Format("{0}/discover/{1}", host, channel);
        Debug.LogFormat("start get server params, {0}", url);
        StartCoroutine(HttpUtil.GetServerMetaInfo(url, OnGetServerParam));
        loginBtn.enabled = false;
    }

    void OnClickStartMatch()
    {
        //开始匹配
        var req = new Proto.StartMatchRoomReq
        {
            // TODO:
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_MATCH, MsgType.CM_START_MATCH, req);
        matchBtn.enabled = false;
    }

    void OnClickStartBattle()
    {
        var req = new Proto.StopMatchBeginGameReq
        {
            // TODO:
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_MATCH, MsgType.CM_STOP_MATCH, req);
        battleBtn.enabled = false;
    }

    //注册消息回调
    void RegisterHandler()
    {
        var client = Client.Instance;
        client.Attach(MsgType.SM_DISCONNECT_NOTIFY, HandleDisconnectNotify);
        client.Attach(MsgType.SM_CLIENT_HANDSHAKE_STATUS, OnHandShakeComplete);
        client.Attach(MsgType.SM_LOGIN_STATUS, HandleLoginStatus);
        client.Attach(MsgType.SM_ENTER_GAME, HandleEnterMainScreen);
        client.Attach(MsgType.SM_START_MATCH, HandleStartMatchResponse);
        client.Attach(MsgType.SM_JOIN_MATCH_NOTIFY, HandleJoinMatchNotify);
        client.Attach(MsgType.SM_QUIT_MATCH, HandleQuitMatchResponse);
        client.Attach(MsgType.SM_QUIT_MATCH_NOTIFY, HandleQuitMatchNotify);
        client.Attach(MsgType.SM_STOP_MATCH, HandleStopMatchResponse);
        client.Attach(MsgType.SM_MATCH_COMPLETE_NOTIFY, HandleMatchCompleteNotify);
        client.Attach(MsgType.SM_JOIN_BATTLE, HandleJoinBattleResponse);
        client.Attach(MsgType.SM_JOIN_BATTLE_NOTIFY, HandleJoinBattleNotify);
        client.Attach(MsgType.SM_QUIT_BATTLE, HandleQuitBattleResponse);
        client.Attach(MsgType.SM_QUIT_BATTLE_NOTIFY, HandleQuitBattleNotify);
        client.Attach(MsgType.SM_BATTLE_START_NOTIFY, HandleBattleStartNotify);
        client.Attach(MsgType.SM_FRAME_UPDATE_NOTIFY, HandleBattleFrameNotify);
        client.Attach(MsgType.SM_FETCH_FRAME, HandleFetchFrame);
    }

    void OnGetServerParam(Proto.ServerMetaParams param)
    {
        if (param == null)
        {
            Debug.LogErrorFormat("get server params fail");
            return;
        }
        serverParam = param;
        string url = string.Format("{0}/account/register", host);
        Debug.LogFormat("start register account, {0}", url);
        StartCoroutine(HttpUtil.RegisterAccount(url, "", "", OnRegisterAccountCompleted));
    }

    void OnRegisterAccountCompleted(Proto.RegisterAccountRes response)
    {
        if (response == null || response.Status != 0)
        {
            Debug.LogErrorFormat("register account fail, {0}", response.Status);
            return;
        }
        authToken = response.Token;
        Debug.LogFormat("start get server list, {0}", serverParam.ServersUrl);
        StartCoroutine(HttpUtil.GetServerList(serverParam.ServersUrl, OnGetServerList));
    }

    void OnGetServerList(Proto.ServerInfoList serverList)
    {
        if (serverList == null || serverList.Servers.Count == 0)
        {
            Debug.LogErrorFormat("get server params fail");
            return;
        }
        for (int i = 0; i < serverList.Servers.Count; i++)
        {
            var item = serverList.Servers[i];
            if (item.Status != Proto.ServerStatusType.StatusMaintain)
            {
                Debug.LogFormat("start login server, {0}", item.LoginUrl);
                StartCoroutine(HttpUtil.LoginServer(item.LoginUrl, authToken, channel, OnLoginServerCompleted));
                break;
            }
        }
    }

    void OnLoginServerCompleted(Proto.UserAuthenticateRes res)
    {
        if (res.Status != 0)
        {
            Debug.LogErrorFormat("login failed, {0}", res.Status);
            return;
        }
        accessToken = res.AccessToken;
        Debug.LogFormat("start connect game, {0}", res.GateAddr);
        
        StartConnectGame(res.GateAddr);
    }

    void StartConnectGame(string serverAddr)
    {
        if (username == "")
        {
            username = SystemInfo.deviceUniqueIdentifier;
        }

        Client.Instance.Connect(serverAddr);
        RegisterHandler();

        Debug.LogFormat("start hand shake");
        var handShakeReq = new Proto.ClientHandShakeReq
        {
            // TODO: encryption
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_GATEWAY, MsgType.CM_CLIENT_HANDSHAKE, handShakeReq);
    }

    void OnHandShakeComplete(Packet pkt)
    {
        var resp = pkt.Parse<Proto.ClientHandShakeRes>();
        Debug.LogFormat("hand shake response: {0}", resp.Method);

        var req = new Proto.ClientLoginReq()
        {
            AccessToken = accessToken,
            Timestamp = Utils.CurrentTimeMillis(),
            Username = username,
            AppChannel = channel,
            ClientOs = SystemInfo.operatingSystem,
            DeviceType = SystemInfo.deviceModel,
            DeviceId = SystemInfo.deviceUniqueIdentifier,
            Language = Application.systemLanguage.ToString(),
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_GATEWAY, MsgType.CM_LOGIN, req);
    }

    //处理登陆返回
    void HandleLoginStatus(Packet pkt)
    {
        var resp = pkt.Parse<Proto.ClientLoginRes>();
        Debug.LogFormat("login response: {0}", resp);

        var req = new Proto.EnterGameReq
        {
            // TODO: nickname
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_CORE, MsgType.CM_ENTER_GAME, req);
    }

    //进入主界面
    void HandleEnterMainScreen(Packet pkt)
    {
        var resp = pkt.Parse<Proto.EnterGameAck>();
        Debug.LogFormat("enter game: {0}", resp.Status);

    }

    void HandleDisconnectNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.DisconnectNotify>();
        Debug.LogFormat("server disconnected: {0}", notify.Reason);
    }

    //
    void HandleStartMatchResponse(Packet pkt)
    {
        var resp = pkt.Parse<Proto.StartMatchRoomAck>();
        Debug.LogFormat("match response: {0}", resp.RoomId);
    }

    void HandleJoinMatchNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.JoinMatchRoomNotify>();
        Debug.LogFormat("match notify: {0}", notify.Player.Nickname);
    }

    void HandleQuitMatchResponse(Packet pkt)
    {
        var resp = pkt.Parse<Proto.QuitMatchRoomAck>();
        Debug.LogFormat("quit match response: {0}", resp.Status);
    }

    void HandleQuitMatchNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.QuitMatchRoomNotify>();
        Debug.LogFormat("quit match notify: {0}", notify.RoleId);
    }

    void HandleStopMatchResponse(Packet pkt)
    {
        var resp = pkt.Parse<Proto.StopMatchBeginGameAck>();
        Debug.LogFormat("stop match response: {0}", resp.Status);
    }

    void HandleMatchCompleteNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.MatchCompleteNotify>();
        Debug.LogFormat("match complete notify: {0}", notify.RoomId);

        var req = new Proto.JoinBattleReq
        {
        };
        Client.Instance.SendMessage(ServiceType.SERVICE_BATTLE, MsgType.CM_JOIN_BATTLE, req);
    }

    void HandleJoinBattleResponse(Packet pkt)
    {
        var resp = pkt.Parse<Proto.JoinBattleAck>();
        Debug.LogFormat("join battle response: {0}", resp.ReadyPlayerCount);
    }

    void HandleJoinBattleNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.JoinBattleNotify>();
        Debug.LogFormat("join battle notify: {0}", notify.Player.Nickname);
    }

    void HandleQuitBattleResponse(Packet pkt)
    {
        var resp = pkt.Parse<Proto.QuitBattleAck>();
        Debug.LogFormat("quit battle response: {0}", resp.Status);
    }

    void HandleQuitBattleNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.QuitBattleNotify>();
        Debug.LogFormat("quit battle notify: {0}", notify.RoleId);
    }

    void HandleBattleStartNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.BattleStartNotify>();
        Debug.LogFormat("battle start notify: {0}", notify.Seed);
    }

    void HandleBattleFrameNotify(Packet pkt)
    {
        var notify = pkt.Parse<Proto.SyncFrameNotify>();
        Debug.LogFormat("sync frame notify: {0}", notify.Frames.Count);
    }

    void HandleFetchFrame(Packet pkt)
    {
        var resp = pkt.Parse<Proto.FetchFrameAck>();
        Debug.LogFormat("fetch frame response: {0}", resp.Frames.Count);
    }
}
