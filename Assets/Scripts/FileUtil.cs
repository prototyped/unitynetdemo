using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class FileUtil
{
    public static IEnumerator LoadAssetContent(string relativePath, Action<bool, string> cb)
    {
        string absFilePath = Path.Combine(Application.streamingAssetsPath, relativePath);
#if UNITY_ANDROID
        var www = UnityWebRequest.Get(absFilePath);
        www.SendWebRequest();
        yield return www;

        if (www.isHttpError || www.isNetworkError)
        {
            cb(false, www.error);
        }
        else
        {
            cb(true, www.downloadHandler.text);
        }
#else
        yield return null;
        using (var reader = new StreamReader(absFilePath, Encoding.UTF8))
        {
            string content = reader.ReadToEnd();
            cb(true, content);
        }
#endif
    }
}