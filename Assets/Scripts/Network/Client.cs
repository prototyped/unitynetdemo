﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.


using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    class RpcContext
    {
        public ushort command = 0;
        public ushort reply = 0;
        public uint sequence = 0;
        public long deadline = 0;
        public Action<Packet> cb;
    };

    // TCP net client
    public class Client
    {
        private Ticker ticker = new Ticker();
        private Dictionary<MsgType, Action<Packet>> handlers = new Dictionary<MsgType, Action<Packet>>();
        private Dictionary<ushort, bool> registry = new Dictionary<ushort, bool>();
        private Dictionary<uint, RpcContext> pending = new Dictionary<uint, RpcContext>();

        private Queue<Packet> incomming = new Queue<Packet>(128);
        private Queue<Exception> errors = new Queue<Exception>(8);

        private TcpEndpoint endpoint;

        private long cachedTime = 0;              // cached current time
        public uint LastSeqno { get; set; }
        public ushort District { get; set; }


        public Client()
        {
            District = 1;
        }

        public void Attach(MsgType command, Action<Packet> cb)
        {
            if (handlers.ContainsKey(command))
            {
                handlers[command] += cb;
            }
            else
            {
                handlers[command] = cb;
            }
        }

        public void Detach(MsgType command)
        {
            handlers.Remove(command);
        }

        public void Connect(string address)
        {
            string host = "";
            ushort port = 0;
            Utils.SplitHostPort(address, ref host, ref port);
            endpoint = new TcpEndpoint(new V1Codec());
            endpoint.Connect(host, port);
            endpoint.StartRead(incomming, errors);
        }

        public void Close()
        {
            if (endpoint != null)
            {
                endpoint.Close();
                endpoint = null;
            }
        }

        public void StartHeartbeat()
        {
            Attach(MsgType.SM_HEARTBEAT_STATUS, HandleHeartBeat);
            ticker.Schedule((int)Constant.DefaultHeartBeatSec, true, HeartBeat);
            ticker.Schedule(1000, true, ReapRpcReply);
        }

        public void SendMessage(ServiceType dest, MsgType cmd, Google.Protobuf.IMessage msg)
        {
            uint node = (((uint)dest << 8) | District);
            var pkt = new Packet()
            {
                Node = node,
                Command = (ushort)cmd,
                SeqNo = LastSeqno++,
                Message = msg,
                Flags = (ushort)Constant.PacketFlagTypeProto,
            };
            var data = Utils.SerializeProtoToBytes(msg);
            var dataLength = 0;
            if (data != null && data.Length > 0)
            {
                dataLength = data.Length;
                // set exposible to true, so we can use GetBuffer()
                pkt.BodyStream = new MemoryStream(data, 0, data.Length, true, true);
            }
            SendPacket(pkt);

#if UNITY_STANDALONE
            if (cmd != MsgType.CM_HEARTBEAT && cmd != MsgType.CM_INPUT_FRAME)
            {
                Debug.LogFormat("Send message: {0}, {1}", cmd, Utils.PrettyBytes((int)dataLength));
            }
#endif
        }

        public void SendBytes(ServiceType dest, MsgType cmd, byte[] data, int offset = 0, int size = -1)
        {
            if (size < 0)
            {
                size = data.Length;
            }
            var stream = new MemoryStream(data, offset, size);
            SendStream(dest, cmd, stream);
        }

        public void SendStream(ServiceType dest, MsgType cmd, MemoryStream stream)
        {
            uint node = (((uint)dest << 8) | District);
            var pkt = new Packet()
            {
                Node = node,
                Command = (ushort)cmd,
                SeqNo = LastSeqno++,
                Flags = (ushort)Constant.PacketFlagTypeBinary,
                BodyStream = stream,
            };
            SendPacket(pkt);
        }

        public void SendPacket(Packet pkt)
        {
            var service = (pkt.Node >> 8);
            if (service == (int)ServiceType.SERVICE_CLIENT)
            {
                incomming.Enqueue(pkt);
            }
            else
            {
                endpoint.SendPacket(pkt);
            }
        }

        public void Call(ServiceType service, MsgType request, MsgType reply, Google.Protobuf.IMessage msg, Action<Packet> cb)
        {
            if (!registry.ContainsKey((ushort)reply))
            {
                registry.Add((ushort)reply, true);
                Attach(reply, OnRpcDone);
            }
            long now = Utils.CurrentTimeMillis();
            var ctx = new RpcContext
            {
                command = (ushort)request,
                reply = (ushort)reply,
                sequence = LastSeqno,
                deadline = now + (long)Constant.DefaultReplyTtl,
                cb = cb,
            };
            pending[ctx.sequence] = ctx;
            SendMessage(service, request, msg);
        }

        void OnRpcDone(Packet pkt)
        {
            RpcContext ctx;
            if (!pending.TryGetValue(pkt.SeqNo, out ctx))
            {
                Debug.LogErrorFormat("{0} rpc {1} context expired", pkt.Command, pkt.SeqNo);
                return;
            }
            pending.Remove(pkt.SeqNo);
            ctx.cb(pkt);
        }

        void ReapRpcReply()
        {
            var now = Utils.CurrentTimeMillis();
            while (true)
            {
                if (pending.Count == 0)
                    break;
                foreach (var item in pending)
                {
                    var ctx = item.Value;
                    if (now > ctx.deadline)
                    {
                        var pkt = new Packet
                        {
                            Command = ctx.reply,
                            Errno = (uint)Constant.ErrRequestTimedout,
                            SeqNo = ctx.sequence,
                            Flags = (ushort)Constant.PacketFlagError,
                        };
                        Dispatch(pkt);
                    }
                }
            }
        }

        void HeartBeat()
        {
            var req = new Proto.HeartbeatReq
            {
                Time = (ulong)cachedTime
            };
            SendMessage(ServiceType.SERVICE_GATEWAY, MsgType.CM_HEARTBEAT, req);
        }

        void HandleHeartBeat(Packet pkt)
        {
            //var resp = pkt.Parse<Proto.HeartbeatPong>();
        }

        // cached time
        public long CurrentTime()
        {
            return cachedTime;
        }

        public void Dispatch(Packet pkt)
        {
            var command = (MsgType)pkt.Command;
            Action<Packet> callback;
            if (!handlers.TryGetValue(command, out callback))
            {
                Debug.LogErrorFormat("Message {0} handler not found", command);
                return;
            }
            try
            {
                if (command != MsgType.SM_HEARTBEAT_STATUS && command != MsgType.SM_FRAME_UPDATE_NOTIFY)
                {
                    Debug.LogFormat("Recv message {0}, {1}", command, Utils.PrettyBytes(pkt.BytesCount));
                }
                if (pkt.GetError() != 0)
                {
                    Debug.LogErrorFormat("message {0} error: {1}", command, pkt.GetError());
                }
                else
                {
                    callback(pkt);
                }
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("net exception: {0}", ex.ToString());
            }
        }

        void ProcessMessage()
        {
            while (incomming.Count > 0)
            {
                var pkt = incomming.Dequeue();
                Dispatch(pkt);
            }
            if (errors.Count > 0)
            {
                var err = errors.Dequeue();
                Debug.LogErrorFormat("net error: {0}", err);
                Close();
            }
            incomming.Clear();
            errors.Clear();
        }

        // Dispatch messages
        public void Run()
        {
            var now = Utils.CurrentTimeMillis();
            cachedTime = now;
            ticker.Tick(now);
            ProcessMessage();
            if (endpoint != null)
            {
                endpoint.Update();
            }
        }

        public static Client Instance { get; private set; }

        public static void Initialize()
        {
            Instance = new Client();
        }

        public static void Update()
        {
            if (Instance != null)
            {
                Instance.Run();
            }
        }

        public static void Destroy()
        {
            Instance.Close();
            Instance = null;
        }
    }
}
