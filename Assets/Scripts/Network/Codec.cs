// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.IO;
using UnityEngine;

namespace Network
{
    public interface ICodec
    {
        // Packet header bytes 
        int HeaderByteSize { get; }

        // Encode packet to bytes
        MemoryStream Encode(Packet pkt);

        // Decode a packet from bytes buffer
        int Decode(Packet pkt, byte[] data, int offset, int size);

        // Get how many bytes this packet needs
        int GetDecodeLength(byte[] data, int offset, int size);
    }

    // packet header wire layout, 20 bytes
    //       ----------------------------------------
    // field | len |  cmd | flag | seq | node | crc |
    //       ----------------------------------------
    // bytes |  4  |   2  |  2   |  4  |  4   |  4  |
    //       ----------------------------------------
    public class V1Codec : ICodec
    {
        public int HeaderByteSize { get { return 20; } }
        
        private Compressor compressor = new Compressor();
        private Crc32Algorithm crc32 = new Crc32Algorithm();

        public V1Codec()
        {
        }

        void EncodeHeader(Packet pkt, int bodyLen, MemoryStream stream)
        {
            int offset = 0;
            var buffer = new byte[HeaderByteSize];
            bodyLen |= (int)Constant.ProtocolVersion << (int)Constant.VersionBitsShift;
            LittleEndian.WriteU32(buffer, ref offset, (uint)bodyLen);
            LittleEndian.WriteU16(buffer, ref offset, pkt.Command);
            LittleEndian.WriteU16(buffer, ref offset, pkt.Flags);
            LittleEndian.WriteU32(buffer, ref offset, pkt.SeqNo);
            LittleEndian.WriteU32(buffer, ref offset, pkt.Node);
            LittleEndian.WriteU32(buffer, ref offset, pkt.Errno);
            stream.Write(buffer, 0, buffer.Length);
        }

        // Encode packet to bytes array
        public MemoryStream Encode(Packet pkt)
        {
            uint checksum = 0;
            int byteSize = HeaderByteSize;
            int bodyLen = 0;
            if (pkt.BodyStream != null)
            {
                bodyLen = (int)pkt.BodyStream.Length;
                if (bodyLen > 0)
                {
                    if (bodyLen > (int)Constant.MaxPacketPayLoad)
                    {
                        var msg = string.Format("V2Codec: {0} payload size[{1}] out of range", pkt.Command, bodyLen);
                        throw new ArgumentOutOfRangeException(msg);
                    }
                    byteSize += bodyLen;
                    checksum = crc32.Compute(pkt.BodyStream.GetBuffer(), 0, bodyLen);
                }
            }
            pkt.Errno = checksum;
            var stream = new MemoryStream(byteSize);
            EncodeHeader(pkt, bodyLen, stream);
            if (pkt.BodyStream != null)
            {
                pkt.BodyStream.WriteTo(stream);
            }
            return stream;
        }

        // Get how many bytes this packet needs
        public int GetDecodeLength(byte[] data, int offset, int size)
        {
            if (size < 4)
            {
                return -1;
            }
            var length = LittleEndian.ReadU32(data, ref offset);
            length &= (uint)Constant.VersionBitsMask;
            return (int)length + HeaderByteSize;
        }

        // // Decode a packet from bytes buffer
        public int Decode(Packet pkt, byte[] data, int offset, int size)
        {
            if (size < 4)
            {
                return -1;
            }
            var length = LittleEndian.ReadU32(data, ref offset);
            length &= (int)Constant.VersionBitsMask;
            if (HeaderByteSize + length > size)
            {
                return -2;
            }
            pkt.Command = LittleEndian.ReadU16(data, ref offset);
            pkt.Flags = LittleEndian.ReadU16(data, ref offset);
            pkt.SeqNo = LittleEndian.ReadU32(data, ref offset);
            pkt.Node = LittleEndian.ReadU32(data, ref offset);
            pkt.Errno = LittleEndian.ReadU32(data, ref offset);
            pkt.BytesCount = (int)(HeaderByteSize + length);
            if (length == 0)
            {
                return 0;
            }

            var checksum = crc32.Compute(data, offset, (int)length);
            if (checksum != pkt.Errno)
            {
                Debug.LogFormat("Decode packet {0}, checksum mismatch, {1} != {2}", pkt.Command, checksum, pkt.Errno);
                return -3;
            }
            if ((pkt.Flags & (int)Constant.PacketFlagCompressed) > 0)
            {
                var uncompressed = new MemoryStream();
                compressor.UnCompress(data, offset, (int)length, uncompressed);
                uncompressed.Seek(0, SeekOrigin.Begin);
                pkt.BodyStream = uncompressed;
            }
            else
            {
                // `data` is a global buffer, so we need make a copy
                var stream = new MemoryStream((int)length);
                stream.Write(data, offset, (int)length);
                stream.Seek(0, SeekOrigin.Begin);
                pkt.BodyStream = stream;
            }
            return 0;
        }
    }
}
