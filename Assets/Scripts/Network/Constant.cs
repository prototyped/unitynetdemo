﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.


namespace Network
{
    public enum Constant
    {
        ProtocolVersion = 6,                // protocol version
        VersionBitsShift = 28,              //
        VersionBitsMask = 0x0FFFFFFF,       //
        CompressThreshold = 1024,           // 1k
        MaxPacketPayLoad = 1024 * 1024,     // 1M

        DefaultHeartBeatSec = 10,           // heartbeat interval
        DefaultReplyTtl = 45,               // RPC timeout
        TCPRecvTimeoutSeconds = 60,         //
        HTTPTimeout = 30,                   //

        // lower 8 bits are ignored duration encoding
        PacketFlagPlain       = 0x0001, // 消息头明文传输
        PacketFlagCompressed  = 0x0002, // 数据体经过zip压缩
        PacketFlagError       = 0x0100, // 数据请求失败
        PacketFlagTypeProto   = 0x1000, //
        PacketFlagTypeText    = 0x2000, //
        PacketFlagTypeBinary  = 0x4000, //

        PacketFlagBitsMask = 0xFF00, //remove low 8bits flags

        ErrRequestTimedout = 10051,
    }

    public enum ServiceType
    {
        SERVICE_CLIENT = 0x00,          //发送给客户端自己
        SERVICE_LOGIN = 0x02,           //登陆服务
        SERVICE_GATEWAY = 0x03,         //网关服务
        SERVICE_CORE = 0x05,            //核心单人玩法
        SERVICE_MATCH = 0x06,           //匹配服务
        SERVICE_BATTLE = 0x07,          //战斗服务
    };
}
