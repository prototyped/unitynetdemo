// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    public class TcpEndpoint
    {        
        private Queue<Exception> errors = new Queue<Exception>();
        private Queue<Packet> inboundBuf = new Queue<Packet>(8);
        private Queue<Packet> inbox;
        private Transport transport;            // tansport object
        private Socket socket;

        public ICodec Codec { get; set; }

        public TcpEndpoint(ICodec codec)
        {
            Codec = codec;
        }

        public void Close()
        {
            if (socket != null)
            {
                socket.Close();
                socket = null;
            }
        }

        public void Connect(string host, ushort port)
        {
            IPAddress[] addrList = Dns.GetHostAddresses(host);
            if (addrList.Length > 0)
            {
                var addr = addrList[0];
                socket = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(new IPEndPoint(addr, port));
                socket.ReceiveTimeout = (int)Constant.TCPRecvTimeoutSeconds;
                socket.NoDelay = true;
                transport = new Transport(socket, Codec);
            }
            else
            {
                var msg = string.Format("Cannot resolve host {0}", host);
                throw new InvalidOperationException(msg);
            }
        }

        public void StartRead(Queue<Packet> inbound, Queue<Exception> err)
        {
            if (transport != null)
            {
                inbox = inbound;
                errors = err;
                transport.StartRead();
            }
            else
            {
                Debug.Log("StartRead: transport is null");
            }
        }

        public void SendPacket(Packet pkt)
        {
            if (!socket.Connected)
            {
                var desc = string.Format("SendPacket: {0} socket already closed", pkt.Command);
                errors.Enqueue(new InvalidOperationException(desc));
                return;
            }
            if (transport == null)
            {
                Debug.LogFormat("SendPacket: transport is null");
                return;
            }
            try
            {
                var stream = Codec.Encode(pkt);
                transport.SendStreamBytes(stream);
            }
            catch (Exception ex)
            {
                errors.Enqueue(ex);
            }
        }

        public void Update()
        {
            if (transport == null)
            {
                return;
            }
            transport.SwitchErrorQueue(ref errors);
            transport.SwapInboxQueue(ref inboundBuf);
            while (inboundBuf.Count > 0)
            {
                inbox.Enqueue(inboundBuf.Dequeue());
            }
        }
    }
}