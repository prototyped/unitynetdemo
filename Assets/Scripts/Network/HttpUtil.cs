// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#define UNITY_HTTP

using System;
using System.Text;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


namespace Network
{  
    public class HttpUtil
    {
        public static int DefaultTimeout = 60;

        static public byte[] HttpGet(string url)
        {
            var request = WebRequest.CreateHttp(url);
            request.Method = "GET";
            request.Timeout = DefaultTimeout * 1000;
            using (var response = request.GetResponse())
            {
                var stream = response.GetResponseStream();
                var data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                stream.Close();
                return data;
            }
        }

        static public byte[] HttpPost(string url, byte[] body, int index, int size)
        {
            var request = WebRequest.CreateHttp(url);
            request.Method = "POST";
            request.Timeout = DefaultTimeout * 1000;
            request.ContentType = "application/octet-stream";
            var outstream = request.GetRequestStream();
            outstream.Write(body, index, size);
            outstream.Close();
            using (var response = request.GetResponse())
            {
                var stream = response.GetResponseStream();
                var data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                stream.Close();
                return data;
            }
        }

        static public IEnumerator UnityHttpGet(string url, Action<string, byte[]> cb)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                www.chunkedTransfer = false;
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.LogErrorFormat("UnityHttpGet: web request error: {0} {1} {2}", www.responseCode, www.error, url);
                    cb(www.error, null);
                }
                else
                {
                    cb("", www.downloadHandler.data);
                }
            }
        }

        static public IEnumerator UnityHttpPost(string url, Dictionary<string, string> form, Action<string, byte[]> cb)
        {
            using (UnityWebRequest www = UnityWebRequest.Post(url, form))
            {
                www.chunkedTransfer = false;
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.LogErrorFormat("UnityHttpPost: web request error: {0} {1} {2}", www.responseCode, www.error, url);
                    cb(www.error, null);
                }
                else
                {
                    cb("", www.downloadHandler.data);
                }
            }
        }

        //获取url信息
        static public IEnumerator GetServerMetaInfo(string url, Action<Proto.ServerMetaParams> cb)
        {
#if UNITY_HTTP
            yield return UnityHttpGet(url, (error, data) => 
            {
                if (data != null)
                {
                    var msg = Utils.ParseProtoFromBytes<Proto.ServerMetaParams>(data);
                    Debug.LogFormat("GetServerMetaInfo: {0}, {1}, {2}", msg.ServersUrl, msg.NoticeUrl, msg.ResUpdateUrl);
                    cb(msg);
                }
            });
#else
            var data = HttpGet(url);
            yield return null;
            var msg = Utils.ParseProtoFromBytes<Proto.ServerMetaParams>(data);
            Debug.LogFormat("GetServerMetaInfo: {0}, {1}, {2}", msg.ServersUrl, msg.NoticeUrl, msg.ResUpdateUrl);
            cb(msg);
#endif
        }

        //获取服务器列表
        static public IEnumerator GetServerList(string url, Action<Proto.ServerInfoList> cb)
        {
#if UNITY_HTTP
            yield return UnityHttpGet(url, (error, data) =>
            {
                if (data != null)
                {
                    var msg = Utils.ParseProtoFromBytes<Proto.ServerInfoList>(data);
                    Debug.LogFormat("GetServerList: {0}", msg.Servers.Count);
                    cb(msg);
                }
            });
#else
            var data = HttpGet(url);
            yield return null;
            var msg = Utils.ParseProtoFromBytes<Proto.ServerInfoList>(data);
            Debug.LogFormat("GetServerList: {0}", msg.Servers.Count);
            cb(msg);
#endif
        }

        //账号注册认证
        static public IEnumerator RegisterAccount(string url, string account, string passwd, Action<Proto.RegisterAccountRes> cb)
        {
            if (account == "")
            {
                account = SystemInfo.deviceUniqueIdentifier;
            }
            if (passwd.Length == 0)
            {
                var namedata = Encoding.UTF8.GetBytes(account);
                passwd = Utils.SHA1Sum(namedata, 0, namedata.Length);
            }
            var request = new Proto.RegisterAccountReq
            {
                Account = account,
                HashText = passwd,
                ClientOs = SystemInfo.operatingSystem,
                DeviceType = SystemInfo.deviceModel,
                DeviceId = SystemInfo.deviceUniqueIdentifier,
                Language = Application.systemLanguage.ToString(),
            };

#if UNITY_HTTP
            var rawdata = Google.Protobuf.JsonFormatter.Default.Format(request);
            var form = new Dictionary<string, string>
            {
                { "pb-data", rawdata }
            };
            yield return UnityHttpPost(url, form, (error, data) =>
            {
                if (data != null)
                {
                    var msg = Utils.ParseProtoFromBytes<Proto.RegisterAccountRes>(data);
                    Debug.LogFormat("RegisterAccount: {0}, {1}", msg.Status, msg.Token);
                    cb(msg);
                }
            });
#else
            var data = Utils.SerializeProtoToBytes(request);
            var respData = HttpPost(url, data, 0, data.Length);
            yield return null;
            var msg = Utils.ParseProtoFromBytes<Proto.RegisterAccountRes>(respData);
            Debug.LogFormat("RegisterAccount: {0}, {1}", msg.Status, msg.Token);
            cb(msg);
#endif
        }

        //登录
        static public IEnumerator LoginServer(string url, string token, string channel, Action<Proto.UserAuthenticateRes> cb)
        {
            var request = new Proto.UserAuthenticateReq
            {
                ProtocolVer = (uint)Constant.ProtocolVersion,
                Channel = channel,
                Token = token,
            };

#if UNITY_HTTP
            var rawdata = Google.Protobuf.JsonFormatter.Default.Format(request);
            var form = new Dictionary<string, string>
            {
                { "pb-data", rawdata }
            };
            yield return UnityHttpPost(url, form, (error, data) =>
            {
                if (data != null)
                {
                    var msg = Utils.ParseProtoFromBytes<Proto.UserAuthenticateRes>(data);
                    Debug.LogFormat("LoginServer: {0}, {1}#{2}, {3}", msg.Status, msg.Username, msg.UserId, msg.GateAddr);
                    cb(msg);
                }
            });
#else
            var data = Utils.SerializeProtoToBytes(request);
            var respData = HttpPost(url, data, 0, data.Length);
            yield return null;
            var msg = Utils.ParseProtoFromBytes<Proto.UserAuthenticateRes>(respData);
            Debug.LogFormat("LoginServer: {0}, {1}#{2}, {3}", msg.Status, msg.Username, msg.UserId, msg.GateAddr);
            cb(msg);
#endif
        }
    }
}
