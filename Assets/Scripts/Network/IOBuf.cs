﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;

namespace Network
{
    // A buffer class modeled after org.jboss.netty.buffer.ChannelBuffer
    //
    // @see https://github.com/chenshuo/muduo/blob/master/muduo/net/Buffer.h
    //
    // +-------------------+------------------+------------------+
    // | prependable bytes |  readable bytes  |  writable bytes  |
    // |                   |     (CONTENT)    |                  |
    // +-------------------+------------------+------------------+
    // |                   |                  |                  |
    // 0      <=      readerIndex   <=   writerIndex    <=     size

    public class IOBuffer
    {
        public byte[] DataBytes { get; private set; }
        public int ReadIndex { get; set; }
        public int WriteIndex { get; set; }

        public IOBuffer(int initSize)
        {
            if (initSize <= 0)
            {
                initSize = 1024;
            }
            DataBytes = new byte[initSize];
        }

        public int WritableBytes()
        {
            return DataBytes.Length - WriteIndex;
        }

        public int ReadableBytes()
        {
            return WriteIndex - ReadIndex;
        }

        // Write bytes from `data` to buffer
        public void Append(byte[] data, int offset, int size)
        {
            EnsureWritableBytes(size);
            Buffer.BlockCopy(data, offset, DataBytes, WriteIndex, size);
            WriteIndex += size;
        }

        // Read bytes from buffer to `data`
        public int Read(byte[] data, int offset, int size)
        {
            int readable = ReadableBytes();
            int bytesRead = (size > readable ? readable : size);
            Buffer.BlockCopy(DataBytes, ReadIndex, data, offset, bytesRead);
            ReadIndex += bytesRead;
            return bytesRead;
        }

        public void Retrieve(int size)
        {
            if (size < ReadableBytes())
            {
                ReadIndex += size;
            }
            else
            {
                ReadIndex = 0;
                WriteIndex = 0;
            }
        }

        public void EnsureWritableBytes(int size)
        {
            if (size <= WritableBytes())
            {
                return;
            }
            int readable = ReadableBytes();
            if (WritableBytes() >= size) // move space
            {
                Buffer.BlockCopy(DataBytes, ReadIndex, DataBytes, 0, readable);
            }
            else // make more space
            {
                int newsize = 64;
                while (newsize < size) // alignment
                {
                    newsize *= 2;
                }
                var newbuf = new byte[DataBytes.Length + newsize];
                Buffer.BlockCopy(DataBytes, ReadIndex, newbuf, 0, readable);
                DataBytes = newbuf;
            }
            ReadIndex = 0;
            WriteIndex = ReadIndex + readable;
        }

        public Packet ReadPacket(ICodec codec)
        {
            //int bytes = ReadableBytes();
            //if (bytes >= codec.HeaderByteSize)
            //{
            //    var pkt = codec.DecodePacket(DataBytes, ReadIndex, ref bytes);
            //    Retrieve(bytes);
            //    return pkt;
            //}
            return null;
        }
    }
}
