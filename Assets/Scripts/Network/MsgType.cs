// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

namespace Network
{
    public enum MsgType
    {
        SM_DISCONNECT_NOTIFY              = 3001, //
        CM_HEARTBEAT                      = 3002, // 心跳
        SM_HEARTBEAT_STATUS               = 3003, // 心跳返回
        CM_CLIENT_HANDSHAKE               = 3004, // client hello
        SM_CLIENT_HANDSHAKE_STATUS        = 3005, // server hello
        SM_KEY_EXCHANGE_NOTIFY            = 3006, // 交换密钥
        SM_NOTIFY_ERROR                   = 3007, // 错误码通知
        SM_SHOW_TIPS                      = 3008, // 提示信息
        SM_SHOW_MSGBOX                    = 3009, // MsgBox弹窗
        CM_LOGIN                          = 3101, // 登陆获取网关
        SM_LOGIN_STATUS                   = 3102, // 登陆返回
        CM_ENTER_GAME                     = 3104, // 进入游戏
        SM_ENTER_GAME                     = 3105, // 进入游戏返回
        CM_START_MATCH                  = 4101, // 开始匹配
        SM_START_MATCH                  = 4102, //
        SM_JOIN_MATCH_NOTIFY            = 4103, // 加入匹配通知
        CM_QUIT_MATCH                   = 4104, // 退出匹配
        SM_QUIT_MATCH                   = 4105, //
        SM_QUIT_MATCH_NOTIFY            = 4106, // 退出匹配通知
        CM_STOP_MATCH                   = 4107, // 结束匹配开始游戏
        SM_STOP_MATCH                   = 4108, //
        SM_MATCH_COMPLETE_NOTIFY        = 4109, // 匹配完成通知
        CM_JOIN_BATTLE                  = 4201, // 加入战斗
        SM_JOIN_BATTLE                  = 4202, //
        SM_JOIN_BATTLE_NOTIFY           = 4203, // 通知有人加入了战斗
        CM_QUIT_BATTLE                  = 4204, // 退出战斗
        SM_QUIT_BATTLE                  = 4205, //
        SM_QUIT_BATTLE_NOTIFY           = 4206, // 通知有人退出了战斗
        SM_BATTLE_START_NOTIFY          = 4207, // 开始战斗
        CM_INPUT_FRAME                  = 4211, // 客户端发送控制帧
        SM_INPUT_FRAME                  = 4212, //
        SM_FRAME_UPDATE_NOTIFY          = 4213, // 服务器广播同步帧
        CM_FETCH_FRAME                  = 4221, // 客户端拉取帧
        SM_FETCH_FRAME                  = 4221, //
    }
}
