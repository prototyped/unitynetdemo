﻿// Copyright 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.IO;


namespace Network
{
    // Packet represents an application message
    public class Packet
    {
        public ushort Command { get; set; }
        public ushort Flags { get; set; }
        public uint SeqNo { get; set; }
        public uint Errno { get; set; }
        public uint Node { get; set; }

        public MemoryStream BodyStream { get; set; }
        public Google.Protobuf.IMessage Message { get; set; }
        public int BytesCount { get; set; }

        public uint GetError()
        {
            if ((Flags & (ushort)Constant.PacketFlagError) > 0)
            {
                return Errno;
            }
            return 0;
        }

        public int GetBodySize()
        {
            if (BodyStream != null)
            {
                return (int)BodyStream.Length;
            }
            return 0;
        }

        public T Parse<T>() where T : Google.Protobuf.IMessage<T>, new()
        {
            var msg = new T();
            if (BodyStream != null && BodyStream.Length > 0)
            {
                var input = new Google.Protobuf.CodedInputStream(BodyStream);
                msg.MergeFrom(input);
            }
            return msg;
        }
    }
}
