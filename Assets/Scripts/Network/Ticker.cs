// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.Collections.Generic;

namespace Network
{
    public delegate void TimerDelegate();

    public class Timer
    {
        public int id;
        public int duration;
        public bool repeat;
        public long priority;
        public TimerDelegate cb;

        public bool Less(Timer other)
        {
            return priority < other.priority;
        }
    }

    // Min-Heap timer scheduler
    public class Ticker
    {
        private List<Timer> heap;
        private Dictionary<int, Timer> dict;
        private List<Timer> expired;
        private int nextID;

        public Ticker()
        {
            nextID = 2017;
            heap = new List<Timer>();
            expired = new List<Timer>();
            dict = new Dictionary<int, Timer>();
        }

        // move a node up in the tree, as long as needed; 
        // used to restore heap condition after insertion. O(log(n)) complexity
        void HeapSiftUp(int j)
        {
            while (true)
            {
                var i = (j - 1) / 2; // parent node
                if (i == j || !heap[j].Less(heap[i]))
                {
                    break;
                }
                // swap i with j
                var tmp = heap[i];
                heap[i] = heap[j];
                heap[j] = tmp;

                j = i;
            }
        }

        // move a node down in the tree, similar to sift-up; 
        // used to restore heap condition after deletion or replacement
        bool HeapSiftDown(int i0, int n)
        {
            var i = i0;
            while (true)
            {
                var j1 = 2 * i + 1;
                if ((j1 >= n) || (j1 < 0)) // j1 < 0 after int overflow
                {
                    break;
                }
                var j = j1; // left child
                var j2 = j1 + 1;
                if (j2 < n && !(heap[j1].Less(heap[j2])))
                {
                    j = j2; // right child
                }
                if (!(heap[j].Less(heap[i])))
                {
                    break;
                }

                // swap i with j
                var tmp = heap[i];
                heap[i] = heap[j];
                heap[j] = tmp;

                i = j;
            }
            return i > i0;
        }

        void RemoveHeapItem(int i)
        {
            var n = heap.Count - 1;
            if (n != i)
            {
                // swap n with i
                var tmp = heap[n];
                heap[n] = heap[i];
                heap[i] = tmp;
                HeapSiftDown(i, n);
                HeapSiftUp(i);
            }
            heap.RemoveAt(i);
        }


        public int Schedule(int milsec, bool repeat, TimerDelegate cb)
        {
            var now = Utils.CurrentTimeMillis();
            var id = nextID++;
            var t = new Timer
            {
                id = id,
                duration = milsec,
                repeat = repeat,
                cb = cb,
            };
            t.priority = now + t.duration;
            heap.Add(t);
            HeapSiftUp(heap.Count - 1);
            dict[id] = t;
            return id;
        }

        public bool UnSchedule(int id)
        {
            Timer t;
            if (dict.TryGetValue(id, out t))
            {
                dict.Remove(id);
                for (int i = 0; i < heap.Count; i++)
                {
                    if (heap[i].id == id)
                    {
                        RemoveHeapItem(i);
                        break;
                    }
                }
                return true;
            }
            return false;
        }

        public void Tick(Int64 now)
        {
            for (int i = 0; i < heap.Count; i++)
            {
                var item = heap[i];
                if (now < item.priority)
                {
                    break;
                }
                if (item.repeat)
                {
                    item.priority += item.duration;
                    if (!HeapSiftDown(i, heap.Count))
                    {
                        HeapSiftUp(i);
                    }
                }
                else
                {
                    var n = heap.Count - 1;
                    // swap n with 0
                    var tmp = heap[n];
                    heap[n] = heap[0];
                    heap[0] = tmp;
                    HeapSiftDown(0, n);
                    heap.RemoveAt(n);
                }
                expired.Add(item);
            }
            if (expired.Count > 0)
            {
                for (int i = 0; i < expired.Count; i++)
                {
                    expired[i].cb();
                }
                expired.Clear();
            }
        }
    }
}