// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    internal class SocketStateObject
    {
        public byte[] data;
        public int start;
        public int end;
        public IAsyncResult ar;
        public Exception ex;
    }

    // Transport object do the dirty work of TCP read/write
    internal class Transport
    {
        private const int PacketBufferSize = 1024;

        private Socket socket;
        private object errMtx = new object();
        private object inboxMtx = new object();
        
        private Queue<Exception> errQueue = new Queue<Exception>();
        private Queue<Packet> inboxQueue = new Queue<Packet>(8);
        private IOBuffer recvBuf = new IOBuffer(16384); // 16KB buffer
        private SocketStateObject recvState = new SocketStateObject();
        private ICodec codec;

        public Transport(Socket sock, ICodec cdec)
        {
            socket = sock;
            codec = cdec;
        }

        // enqueue encoutered error
        void EnqueueError(Exception ex)
        {
            lock (errMtx)
            {
                errQueue.Enqueue(ex);
            }
        }

        // Swap error queue with lock
        public void SwitchErrorQueue(ref Queue<Exception> queue)
        {
            lock (errMtx)
            {
                if (errQueue.Count > 0)
                {
                    var tmp = errQueue;
                    errQueue = queue;
                    queue = tmp;
                }
            }
        }

        void EnqueuePacket(Packet pkt)
        {
            lock (inboxMtx)
            {
                inboxQueue.Enqueue(pkt);
            }
        }

        // Swap inbox message queue with lock
        public void SwapInboxQueue(ref Queue<Packet> queue)
        {
            lock (inboxMtx)
            {
                if (inboxQueue.Count > 0)
                {
                    var tmp = inboxQueue;
                    inboxQueue = queue;
                    queue = tmp;
                }
            }
        }

        public void SendStreamBytes(MemoryStream stream)
        {
            SendRawBytes(stream.GetBuffer(), 0, (int)stream.Length);
        }

        public void SendRawBytes(byte[] data, int offset = 0, int size = -1)
        {
            if (size < 0)
            {
                size = data.Length;
            }
            SocketStateObject state = new SocketStateObject
            {
                data = data,
                start = offset,
                end = offset + size,
            };
            socket.BeginSend(state.data, state.start, state.end - state.start, 0, 
                WriteCallback, state);
        }

        // This method is executed on a ThreadPool thread, keep this in mind
        void WriteCallback(IAsyncResult ar)
        {
            SocketStateObject state = (SocketStateObject)ar.AsyncState;
            try
            {
                int bytesSent = socket.EndSend(ar);
                state.start += bytesSent;
                if (state.start < state.end) // not finished yet, retry unsent bytes remain
                {
                    socket.BeginSend(state.data, state.start, state.end - state.start, 0,
                        new AsyncCallback(WriteCallback), state);
                    return;
                }
                // write completed
                state.data = null;
                state.start = 0;
                state.end = 0;
                state.ar = null;
            }
            catch (SocketException ex)
            {
                EnqueueError(ex);
            }
        }

        public void StartRead()
        {
            StartRecvBytes(recvState);
        }

        void StartRecvBytes(SocketStateObject state)
        {
            recvBuf.EnsureWritableBytes(PacketBufferSize);
            state.ex = null;
            state.data = recvBuf.DataBytes;
            state.start = recvBuf.WriteIndex;
            state.end = recvBuf.DataBytes.Length;
            state.ar = socket.BeginReceive(state.data, state.start, state.end - state.start,
                0, new AsyncCallback(RecvCallback), state);
        }

        void RecvCallback(IAsyncResult ar)
        {
            SocketStateObject state = (SocketStateObject)ar.AsyncState;
            try
            {
                int bytesRecv = socket.EndReceive(ar);
                if (bytesRecv <= 0) // EOF
                {
                    var ex = new EndOfStreamException("socket closed by peer");
                    EnqueueError(ex);
                }
                else
                {
                    recvBuf.EnsureWritableBytes(bytesRecv);
                    recvBuf.WriteIndex += bytesRecv;
                    for (int r = 1; r > 0;)
                    {
                        r = TryReadPacket();
                    }
                    StartRecvBytes(recvState);
                }
            }
            catch (SocketException ex)
            {
                EnqueueError(ex);
            }
        }

        int TryReadPacket() 
        {
            int readBytes = recvBuf.ReadableBytes();
            if (readBytes == 0 || readBytes < codec.HeaderByteSize)
            {
                return -1;
            }
            int length = codec.GetDecodeLength(recvBuf.DataBytes, recvBuf.ReadIndex, readBytes);
            if (length == 0 || readBytes < length)
            {
                return -2;
            }

            int r = -1;
            try
            {
                var pkt = new Packet();
                r = codec.Decode(pkt, recvBuf.DataBytes, recvBuf.ReadIndex, length);
                if (r == 0)
                {
                    recvBuf.ReadIndex += length;
                    EnqueuePacket(pkt);
                    return 1;
                }
            }
            catch(Exception ex)
            {
                Debug.LogErrorFormat("Codec.Decode: exception, {0}", ex.ToString());
            }
            return r;
        }
    }
}
