// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

using System;
using System.Net;
using System.Text;
using System.Security.Cryptography;

namespace Network
{
    public class Utils
    {
        private static readonly string hexTable = "0123456789abcdef";
        private static readonly DateTime epoch = new DateTime(1970, 1, 1);

#if UNITY_STANDALONE
        private static Google.Protobuf.JsonFormatter formatter = new Google.Protobuf.JsonFormatter(Google.Protobuf.JsonFormatter.Settings.Default);
        private static Google.Protobuf.JsonParser parser = new Google.Protobuf.JsonParser(Google.Protobuf.JsonParser.Settings.Default);
#endif

        // Binary data to hex string
        public static string BinaryToHex(byte[] data, int offset = 0)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = offset; i < data.Length; i++)
            {
                byte v = data[i];
                sb.Append(hexTable[v >> 4]);
                sb.Append(hexTable[v & 0x0f]);
            }
            return sb.ToString();
        }

        public static string MD5Sum(byte[] data, int offset, int count)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(data, offset, count);
            return BinaryToHex(hash);
        }

        public static string SHA1Sum(byte[] data, int offset, int count)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            var hash = sha1.ComputeHash(data, offset, count);
            return BinaryToHex(hash);
        }

        // Swap two values
        public static void Swap<T>(ref T t1, ref T t2)
        {
            T tmp = t1;
            t1 = t2;
            t2 = tmp;
        }

        // Current datetime to unix timestamp
        public static long ToUnixTimestamp(DateTime t)
        {
            var timespan = t.ToUniversalTime().Subtract(epoch);
            return (long)Math.Truncate(timespan.TotalSeconds);
        }

        // Unix timestamp to local time
        public static DateTime TimestampToLocalTime(long timestamp)
        {
            return epoch.AddSeconds(timestamp);
        }

        // Current wall clock time in milliseconds
        public static long CurrentTimeMillis()
        {
            return Convert.ToInt64(DateTime.UtcNow.Subtract(epoch).TotalMilliseconds);
        }

        public static string EncodeBase62(long value)
        {
            const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            if (value == 0)
            {
                return "";
            }
            var sb = new StringBuilder();
            while (value > 0)
            {
                int rem = (int)(value % 62);
                value = value / 62;
                sb.Append(alphabet[rem]);
            }
            // reverse list
            for (int i = 0, j = sb.Length - 1; i < sb.Length/2 && j > sb.Length/2; i++, j--)
            {
                char tmp = sb[j];
                sb[j] = sb[i];
                sb[i] = tmp;
            }
            return sb.ToString();
        }

        public static string PrettyBytes(int count)
        {
            if (count < 1024)
            {
                return string.Format("{0} B", count);
            }
            else if (count < 1024 * 1024)
            {
                return string.Format("{0} KB", (float)count / 1024);
            }
            else
            {
                return string.Format("{0} MB", (float)count / (1024 * 1024));
            }
        }

        public static void SplitHostPort(string addr, ref string host, ref ushort port)
        {
            int idx = addr.LastIndexOf(':');
            if (idx < 0 || idx == 0 || idx == addr.Length)
            {
                throw new ArgumentException(String.Format("invalid host port {0}", addr));
            }
            port = Convert.ToUInt16(addr.Substring(idx + 1));
            host = addr.Substring(0, idx);
        }

        public static IPAddress GetLocalIPAddress()
        {
            var strHostName = Dns.GetHostName();
            var ipEntry = Dns.GetHostEntry(strHostName);
            foreach(var entry in ipEntry.AddressList)
            {
                if (entry.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) // IPv4
                {
                    return entry;
                }
            }
            return null;
        }

        public static byte[] SerializeProtoToBytes(Google.Protobuf.IMessage msg)
        {
            var bytesSize = msg.CalculateSize();
            if (bytesSize > 0)
            {
                var buffer = new byte[bytesSize];
                var output = new Google.Protobuf.CodedOutputStream(buffer);
                msg.WriteTo(output);
                output.Flush();
                return buffer;
            }
            return null;
        }

        public static T ParseProtoFromBytes<T>(byte[] data, int offset = 0, int size = -1) where T : Google.Protobuf.IMessage<T>, new()
        {
            if (size < 0)
            {
                size = data.Length;
            }
            var msg = new T();
            if (size > 0)
            {
                var input = new Google.Protobuf.CodedInputStream(data, offset, size);
                msg.MergeFrom(input);
            }
            return msg;
        }

        // base64 in IL2CPP, otherwise JSON
        public static string SerializeProtoToText(Google.Protobuf.IMessage msg)
        {
#if UNITY_STANDALONE
            return formatter.Format(msg);
#else
            var data = SerializeProtoToBytes(msg);
            return Convert.ToBase64String(data);
#endif
        }

        public static T ParseProtoFromText<T>(string text) where T : Google.Protobuf.IMessage<T>, new()
        {
#if UNITY_STANDALONE
            return parser.Parse<T>(text);
#else
            var data = Convert.FromBase64String(text);
            return ParseProtoFromBytes<T>(data);
#endif
        }
    }
}
